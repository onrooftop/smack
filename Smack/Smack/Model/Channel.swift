//
//  Channel.swift
//  Smack
//
//  Created by Panupong Kukutapan on 11/12/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

import Foundation


struct Channel {
    public private(set) var channelTitle: String!
    public private(set) var channelDescription: String!
    public private(set) var id: String!
    
    
}
