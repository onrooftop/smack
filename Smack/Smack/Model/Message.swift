//
//  Message.swift
//  Smack
//
//  Created by Panupong Kukutapan on 11/12/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

import Foundation

struct Message {
    public private(set) var message: String!
    public private(set) var userName: String!
    public private(set) var channelId: String!
    public private(set) var userAvatar: String!
    public private(set) var userAvatarColor: String!
    public private(set) var timeStamp: String!
    public private(set) var id: String!
}
